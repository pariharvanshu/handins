# Setup Instructions
1. Create an account on [GitLab](https://gitlab.com/)
1. Fork the [reference hand-in repository](https://gitlab.com/jhilliker/handins)
	1. White "fork" button, top right of project area
1. Set the project visibility of your fork to "private".
	1. Settings
	1. General
	1. Visibility, project features, permissions
	1. Project visibility -> Private
1. Add the instructor and TAs as developers to your project
	1. Settings
	1. Members
	1. member ->
		1. ```jhilliker```
		1. (TA user names to follow)
	1. role -> Developer
1. Report your fork's SSH clone URL to the quiz on d2l/brightspace.
	1. Blue "clone" button, top right of project area
	1. "Clone with SSH"
	1. eg: ```git@gitlab.com:jhilliker/handins.git```

# Assignment instructions

```sh
until isAssignmentDone ; do
	doAnswerQuestions
	git add .   # marks files for commit
	git commit  # commits to local repo
	git push    # pushes commits to server
done
```

# Useful Links

* UNIX
	* [POSIX 2018](https://pubs.opengroup.org/onlinepubs/9699919799/?ou=138568)
	* [linux.die.net - Linux Documentation](https://linux.die.net/)
	* [man7 - Linux man pages](http://man7.org/linux/man-pages/)
	* [GNU Manuals Online](https://www.gnu.org/manual/)
* git
	* [Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)
	* [git slide deck](https://drive.google.com/open?id=1NNQ5yDb8F4p0HEecv0CrbUY38T44yLhV)
